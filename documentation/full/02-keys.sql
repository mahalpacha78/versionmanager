
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

ALTER TABLE `application`
ADD KEY `IDX_APPLI_SVCID` (`service_id`);
ALTER TABLE `application`
ADD CONSTRAINT `FK_APPLI_SVCID` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`);

ALTER TABLE `application_environnement`
ADD PRIMARY KEY (`appli_id`,`env_id`),
ADD KEY `IDX_APP_ENVT_APPID` (`appli_id`),
ADD KEY `IDX_APP_ENVT_ENVID` (`env_id`);
ALTER TABLE `application_environnement`
ADD CONSTRAINT `FK_APPENV_ENV_ID` FOREIGN KEY (`env_id`) REFERENCES `environment` (`id`),
ADD CONSTRAINT `FK_APPENV_APP_ID` FOREIGN KEY (`appli_id`) REFERENCES `application` (`id`);

ALTER TABLE `user`
ADD UNIQUE KEY `UK_USER_NAME` (`username`),
ADD UNIQUE KEY `UK_USER_EMAIL` (`email`);
