window.addEventListener('DOMContentLoaded', event => {
  // Simple-DataTables
  // https://github.com/fiduswriter/Simple-DataTables/wiki
  
  const datatablesSimple = document.getElementById('datatablesSimple');
  const datatablesSimpleNoSearch = document.getElementById('datatablesSimpleNoSearch');
  if (datatablesSimple) {
    new simpleDatatables.DataTable(datatablesSimple, {
      paging: false,
      info: false,
      searchable: true,
      select: false,
      ordering:  false,
      labels: {
        placeholder: "Rechercher...", // The search input placeholder
        noResults: "Aucun résultat correspondant à la recherche", // Message shown when there are no search results
      }
    });
  }
  if (datatablesSimpleNoSearch) {
    new simpleDatatables.DataTable(datatablesSimpleNoSearch, {
      paging: true,
      info: true,
      searchable: false,
      select: false,
      ordering:  true, 
      labels: {
        placeholder: "Rechercher...", // The search input placeholder
        perPage: "{select} entrées par page", // per-page dropdown label
        noRows: "Aucun résultat trouvé", // Message shown when there are no records to show
        info: "{rows} résultats trouvés" //
      },
    });
  }
});
