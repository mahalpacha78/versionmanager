Simple Version manager
========================

__features:__
The "Simple Version manager" is symfony application in charge of :
 * List all applications available in an enterprise or activity
 * define all environment related you want to manage
 * Show versions deployed of an application in an environment

>Keep in mind that this tool must stay simple.

Here are the relations covered by the tool.
![Version relations](version-explain.png)

Requirements:
------------
 * Php ^7.2
 * Symfony ^5.4
 * MariaDB ^10.3
 * PDO-SQLite PHP extension enabled


Installation
------------

__Local WAMP config :__
```xml
alias /version "D:/_dvp/versionmanager/public"
<Directory "D:/_dvp/versionmanager/public">
    Options Indexes FollowSymLinks
    AllowOverride all
  <IfDefine APACHE24>
    Require local
  </IfDefine>
  <IfDefine !APACHE24>
    Order Deny,Allow
AllowOverride all
Require all granted
  </IfDefine>
</Directory>
```

__Srv linux vhost config :__
```xml
Listen 80
ServerName  localhost
<VirtualHost SRV-GOLDO:80>
    ServerAdmin webmaster@localhost
    ServerName version.goldo.com


    <Directory />
    Order deny,allow
    Deny from all
</Directory>

DocumentRoot "/data_shared/applications/administration/"
alias /version /data_shared/applications/administration/versionmanager/public
<Directory /data_shared/applications/administration/versionmanager/public>
    Options Indexes FollowSymLinks
    AllowOverride all
    <IfDefine APACHE24>
        Require local
    </IfDefine>
    <IfDefine !APACHE24>
        Order Deny,Allow
        AllowOverride all
        Require all granted
    </IfDefine>
</Directory>

ErrorLog ${APACHE_LOG_DIR}/error-version.log

# Possible values include: debug, info, notice, warn, error, crit,
# alert, emerg.
LogLevel warn

CustomLog ${APACHE_LOG_DIR}/access-version.log combined


</VirtualHost>
```


Datas
-----

Execute this command to run tests:

```bash
$ php bin/console doctrine:fixtures:load
```

In Dos environment, can follow that procedure (dev dotfile environment):

```bash
cd D:/_dvp/versionmanager
SET php="D:\IDE\composer\php.bat"
SET phpcomposer="D:\IDE\composer\composer.bat"
%php% bin/console cache:clear
%php% bin/console doctrine:database:create
%php% bin/console make:migration
%php% bin/console doctrine:migrations:migrate
%php% bin/console doctrine:fixtures:load
```

> Now you are ready to switch to prod environment, in .env file :

 * APP_ENV=dev becomes APP_ENV=prod

> Once installed, you can connect with folowing credentials:

 - Login : alexadmin
 - Password : alexadmin

## Special thanks to:
  - [Symfony framework](https://symfony.com/)
  - [Theme startbootstrap-sb-admin](https://github.com/startbootstrap/startbootstrap-sb-admin)

## License

The source code for the site is licensed under the MIT license, which you can find in
the MIT-LICENSE.txt file.

## Contributing

To be defined ...

## Documentation - screen captures

 - Main search : applications versions by environment
![Version Home page](version-home.png)

 - Dashboards : applications dashboard
![Version dashboard page](version-dashboard.png)
