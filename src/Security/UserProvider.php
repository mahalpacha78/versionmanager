<?php

namespace App\Security;

use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Utils\Constants;

class UserProvider implements UserProviderInterface
{
    protected $em;
    protected $doctrinemanager;

    public function __construct (EntityManagerInterface $doctrinemanager)
    {
        $this->em = $doctrinemanager;
    }

    /**
    * Loads the user for the given username.
    *
    * @throws UsernameNotFoundException if the user is not found
    * @param string $username The username
    *
    * @return UserInterface
    */
    function loadUserByUsername($username) {
        /** Recuperation des entites */
        $user = $this->em->getRepository ( Constants::REPO_VERSION_USERS )->loadUserByUsername (
            $username, Constants::BOOLEAN_TRUE_CODE);

            if(empty($user)){
                throw new UsernameNotFoundException('');
            }
            $this->user = $user;
            return $user;
    }

    /**
    * Refreshes the user for the account interface.
    *
    * @throws UnsupportedUserException if the account is not supported
    * @param UserInterface $user
    *
    * @return UserInterface
    */
    function refreshUser(UserInterface $user) {
        $userInfos = $this->em->getRepository ( Constants::REPO_VERSION_USERS )->loadUserByUsername (
            $user->getUsername());
            return $userInfos;
        }

        /**
        * Whether this provider supports the given user class
        *
        * @param string $class
        *
        * @return Boolean
        */
        function supportsClass($class) {
            return $class === Constants::REPO_VERSION_USERS;
        }
}
