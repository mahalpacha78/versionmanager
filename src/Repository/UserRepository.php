<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function filtrerFiches()
    {
      $qb = $this->createQueryBuilder('usr');
      return $qb
      ->select('usr')
      ->getQuery()
      ->getResult();
    }

    public function updateFiche(int $pId, string $pUsername, string $pFullname, string $pEmail,
      string $pRolesList)
    {
      $qb = $this->createQueryBuilder('usr');
      $query = $qb->update()
      ->set(' usr.username', ':username')
      ->set(' usr.fullName', ':fullName')
      ->set(' usr.email', ':email')
      ->set(' usr.rolesList', ':roles')
      ->where(' usr.id = :editId')
      ->setParameter('username', $pUsername)
      ->setParameter('fullName', $pFullname)
      ->setParameter('email', $pEmail)
      ->setParameter('roles', $pRolesList)
      ->setParameter('editId', $pId)
      ->getQuery();
      $result = $query->execute();
    }

    public function countAll()
    {
      $qb = $this->createQueryBuilder('usr');
      return $qb
      ->select('count(usr.id)')
      ->getQuery()
      ->getSingleScalarResult();
    }

    public function countUsers(string $pRoleName)
    {
      $qb = $this->createQueryBuilder('usr');
      return $qb
      ->select('count(usr.id)')
      ->where("usr.rolesList LIKE '%$pRoleName%'")
      ->getQuery()
      ->getSingleScalarResult();
    }

    public function loadUserByUsername(string $pLogin, $pEnabled=true)
    {
        $qb = $this->createQueryBuilder('u');
        $query = $qb->select('u')
        ->where('u.username = :username')
        ->andWhere('u.actif = :estActif')
        ->setParameter('username', $pLogin)
        ->setParameter('estActif', $pEnabled)
        ->getQuery();

        return $query->getOneOrNullResult();
    }
}
