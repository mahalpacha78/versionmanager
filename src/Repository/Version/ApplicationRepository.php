<?php
namespace App\Repository\Version;

use App\Entity\Version\Application;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Utils\Constants;


class ApplicationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Application::class);
    }

    public function filtrerFiches()
    {
        $qb = $this->createQueryBuilder('app');
        return $qb
        ->select('app')
        ->getQuery()
        ->getResult();
    }

    public function filtrerVersions()
    {
        $qb = $this->createQueryBuilder('app');
        return $qb
        ->select('app, rel, env')
        ->join('app.applicationenvs', 'rel')
        ->join('rel.environment', 'env')
        ->getQuery()
        ->getResult();
    }

    /**
     * Search version base upon criterias in form filter
     * @param string $pAppName will be search by name
     * @param string $pSvcName will be search by name
     * @param string $pEnvName will be search by name
     * @param string $pVersion will be search in like '<ver>%' way
     */
    public function searchVersions(string $pAppName, string $pSvcName, string $pEnvName, string $pVersion)
    {
        $qb = $this->createQueryBuilder('app');
        $query = $qb
        ->select('app, rel, env')
        ->join('app.applicationenvs', 'rel')
        ->join('app.service', 'svc')
        ->join('rel.environment', 'env');

        // Adding filter conditions
        if (!empty($pAppName) && $pAppName != Constants::CD_EMPTY) {
           $query->where(' app.name = :appName')
           ->setParameter('appName', $pAppName);
        }
        if (!empty($pSvcName) && $pSvcName != Constants::CD_EMPTY) {
           $query->andWhere(' svc.name = :service')
           ->setParameter('service', $pSvcName);
        }
        if (!empty($pEnvName) && $pEnvName != Constants::CD_EMPTY) {
           $query->andWhere(' env.name = :envName')
           ->setParameter('envName', $pEnvName);
        }
        if (!empty($pVersion) && $pVersion != Constants::CD_EMPTY) {
           $query->andWhere("rel.version LIKE '$pVersion%'");
        }

        return $query->getQuery()->getResult();
    }



    public function getVersionByIds(int $pAppId, int $pEnvId)
    {
        $qb = $this->createQueryBuilder('app');
        return $qb
        ->select('app, rel, env')
        ->join('app.applicationenvs', 'rel')
        ->join('rel.environment', 'env')
        ->where(' app.id = :appId')
        ->andWhere(' env.id = :envId')
        ->setParameter('appId', $pAppId)
        ->setParameter('envId', $pEnvId)
        ->getQuery()
        ->getSingleResult();
    }

    public function deleteVersion(int $pAppId, int $pEnvId)
    {
        $qb = $this->createQueryBuilder('qbDel')
        ->delete(Constants::REPO_VERSION_VERSIONLINK, 'link')
        ->where(' link.application = :appId')
        ->andWhere(' link.environment = :envId')
        ->setParameter('appId', $pAppId)
        ->setParameter('envId', $pEnvId);
        $query = $qb->getQuery();
        return $query->execute();
    }

    public function updateFiche(int $pId, string $pName, string $pDescription, string $pLink )
    {
        $qb = $this->createQueryBuilder('app');
        $query = $qb->update()
        ->set(' app.name', ':name')
        ->set(' app.description', ':description')
        ->set(' app.link', ':link')
        ->where(' app.id = :editId')
        ->setParameter('name', $pName)
        ->setParameter('description', $pDescription)
        ->setParameter('link', $pLink)
        ->setParameter('editId', $pId)
        ->getQuery();
        $result = $query->execute();
    }

    public function updateFicheVersion(int $pAppId, int $pEnvId, string $pName, string $pDescription, \DateTime $pDate, string $pLink )
    {
        $qb = $this->createQueryBuilder('rel');
        $query = $qb->update()
        ->set('rel.version', ':name')
        ->set('rel.description', ':description')
        ->set('rel.link', ':description')
        ->set('rel.date', ':date')
        ->join('app.applicationenvs', 'rel')
        ->join('rel.environment', 'env')
        ->where(' rel.application.id = :appId')
        ->andWhere(' rel.environment.id = :envId')
        ->setParameter('appId', $pAppId)
        ->setParameter('envId', $pEnvId)
        ->getQuery();
        $result = $query->execute();
    }

    public function getApplicationByName(string $pName)
    {
        $qb = $this->createQueryBuilder('app');
        $query = $qb->select('app')
        ->where(' app.name = :name')
        ->setParameter('name', $pName);

        return $query->getQuery()->getSingleResult();
    }

    public function getDistinctNames()
    {
        $qb = $this->createQueryBuilder('app');
        $query = $qb->select('distinct (app.name)')
        ->orderBy('app.name', 'ASC')
        ->getQuery();
        $result = $query->execute();
    }

    public function countAll()
    {
        $qb = $this->createQueryBuilder('app');
        return $qb
        ->select('count(app.id)')
        ->getQuery()
        ->getSingleScalarResult();
    }

}
