<?php
namespace App\Repository\Version;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Utils\Constants;
use App\Entity\Version\AppEnvRelated;

class VersionRepository extends ServiceEntityRepository
{
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, AppEnvRelated::class);
  }

  public function updateFiche(int $pAppId, int $pEnvId, string $pName, string $pDescription, \DateTime $pDate, string $pLink )
  {
    $qb = $this->createQueryBuilder('rel');
    $query = $qb->update()
    ->set('rel.version', ':name')
    ->set('rel.comment', ':description')
    ->set('rel.link', ':link')
    ->set('rel.date', ':date')
    ->where(' rel.application = :appId')
    ->andWhere(' rel.environment = :envId')
    ->setParameter('appId', $pAppId)
    ->setParameter('envId', $pEnvId)
    ->setParameter('name', $pName)
    ->setParameter('description', $pDescription)
    ->setParameter('link', $pLink)
    ->setParameter('date', $pDate)
    ->getQuery();
    $result = $query->execute();
  }

  public function countAll()
  {
    $qb = $this->createQueryBuilder('rel');
    $query = $qb->select($qb->expr()->countDistinct('rel.version') )
    ->getQuery();
    return $query->getSingleScalarResult();
  }


}
