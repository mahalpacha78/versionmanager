<?php
namespace App\Repository\Version;

use App\Entity\Version\Environment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class EnvironmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Environment::class);
    }

    public function filtrerFiches()
    {
      $qb = $this->createQueryBuilder('env');
      return $qb
      ->select('env')
      ->getQuery()
      ->getResult();
    }

    public function updateFiche(int $pId, string $pName, string $pDescription, string $pMachine )
    {
      $qb = $this->createQueryBuilder('env');
      $query = $qb->update()
      ->set(' env.name', ':name')
      ->set(' env.description', ':description')
      ->set(' env.machine', ':machine')
      ->where(' env.id = :editId')
      ->setParameter('name', $pName)
      ->setParameter('description', $pDescription)
      ->setParameter('machine', $pMachine)
      ->setParameter('editId', $pId)
      ->getQuery();
      $result = $query->execute();
    }

    public function getApplicationByName(string $pName)
    {
      $qb = $this->createQueryBuilder('app');
      $query = $qb->select('app')
      ->where(' app.name = :name')
      ->setParameter('name', $pName);

      return $query->getQuery()->getSingleResult();
    }

    public function getDistinctNames()
    {
      $qb = $this->createQueryBuilder('env');
      $query = $qb->select('distinct (env.name)')
      ->orderBy('env.name', 'ASC')
      ->getQuery();
      $result = $query->execute();
    }

    public function countAll()
    {
      $qb = $this->createQueryBuilder('env');
      return $qb
      ->select('count(env.id)')
      ->getQuery()
      ->getSingleScalarResult();
    }
}
