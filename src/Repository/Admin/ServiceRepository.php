<?php
namespace App\Repository\Admin;

use App\Entity\Admin\Service;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Utils\Constants;

class ServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Service::class);
    }

    public function filtrerFiches()
    {
        $qb = $this->createQueryBuilder('svc');
        return $qb
        ->select('svc')
        ->getQuery()
        ->getResult();
    }


    public function getDistinctNames()
    {
        $qb = $this->createQueryBuilder('svc');
        $query = $qb->select('distinct (svc.name)')
        ->orderBy('svc.name', 'ASC')
        ->getQuery();
        $result = $query->execute();
    }

    public function countAll()
    {
        $qb = $this->createQueryBuilder('svc');
        return $qb
        ->select('count(svc.id)')
        ->getQuery()
        ->getSingleScalarResult();
    }

    public function updateFiche(int $pId, string $pName, string $pDescription,
        string $pSociety , string $pManager, \DateTime $pDate)
    {
        $qb = $this->createQueryBuilder('app');
        $query = $qb->update()
        ->set(' app.name', ':name')
        ->set(' app.description', ':description')
        ->set(' app.society', ':society')
        ->set(' app.manager', ':manager')
        ->set(' app.date', ':date')
        ->where(' app.id = :editId')
        ->setParameter('name', $pName)
        ->setParameter('description', $pDescription)
        ->setParameter('society', $pSociety)
        ->setParameter('manager', $pManager)
        ->setParameter('date', $pDate)
        ->setParameter('editId', $pId)
        ->getQuery();
        $result = $query->execute();
    }

}
