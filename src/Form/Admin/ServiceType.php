<?php

namespace App\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Admin\Service;

/**
* Defines the form used to create and manipulate admin applications
*
*/
class ServiceType extends AbstractType
{
  /**
  * {@inheritdoc}
  */
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $builder->add('name', TextType::class);
    $builder->add('society', TextType::class);
    $builder->add('manager', TextType::class);
    $builder->add('date', DateType::class, array(
                'widget' => 'single_text',
    ));
    $builder->add('description', TextareaType::class, [
      'attr' => ['rows' => 4]
    ]);
    // Actions
    $builder->add('save', SubmitType::class, [
      'label' => 'action.save',
    ]);
  }

  /**
  * {@inheritdoc}
  */
  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults([
      'data_class' => Service::class,
      'allow_extra_fields' => true,
    ]);
  }

  public function getBlockPrefix()
  {
      return '';
  }
}
