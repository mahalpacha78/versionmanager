<?php

namespace App\Form\Search;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Version\AppEnvRelated;
use App\Entity\Version\Application;
use App\Entity\Version\Environment;
use App\Entity\Admin\Service;
use App\Repository\Version\ApplicationRepository;
use App\Repository\Version\EnvironmentRepository;
use App\Repository\Admin\ServiceRepository;
use App\Utils\Constants;

/**
* Defines the form used to search a version based upon criterias
*
*/
class VersionFormType extends AbstractType
{
    private $appliRepo;
    private $envRepo;
    private $svcRepo;

    public function __construct(ApplicationRepository $pAppliRepo,
    EnvironmentRepository $pEnvRepo, ServiceRepository $pSvcRepo)
    {
        $this->appliRepo = $pAppliRepo;
        $this->envRepo = $pEnvRepo;
        $this->svcRepo = $pSvcRepo;
    }

    /**
    * {@inheritdoc}
    */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder->add('service', EntityType::class, [
            'class' => Service::class,
            'placeholder' => Constants::CD_LISTE_VALUE_ALL,
            'query_builder' => $this->appliRepo->getDistinctNames(),
            'choice_label' => 'name',
            'required' => false,
            'mapped' => false,
        ]);

        $builder->add('application', EntityType::class, [
            'class' => Application::class,
            'placeholder' => Constants::CD_LISTE_VALUE_ALL,
            'query_builder' => $this->appliRepo->getDistinctNames(),
            'choice_label' => 'name',
            'required' => false,
            'mapped' => false,
        ]);

        $builder->add('environment', EntityType::class, [
            'class' => Environment::class,
            'placeholder' => Constants::CD_LISTE_VALUE_ALL,
            'query_builder' => $this->envRepo->getDistinctNames(),
            'choice_label' => 'name',
            'required' => false,
            'mapped' => false,
        ]);
        $builder->add('version', TextType::class, [
            'required' => false,
            'mapped' => false,
        ]);


        // Actions
        $builder->add('search', SubmitType::class, [
            'label' => 'action.search',
        ]);
    }

    /**
    * No data class related:
    * Binding an entity with a composite primary key to a query is not supported
    */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
            'allow_add' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'vermgr_search_version';
    }
}
