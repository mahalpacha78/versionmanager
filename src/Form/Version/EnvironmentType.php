<?php

namespace App\Form\Version;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Version\Environment;

/**
* Defines the form used to create and manipulate admin environments
*
*/
class EnvironmentType extends AbstractType
{
  /**
  * {@inheritdoc}
  */
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $builder->add('name', TextType::class);
    $builder->add('description', TextareaType::class, [
      'attr' => ['rows' => 4]
    ]);
    $builder->add('machine', TextType::class);
    // Actions
    $builder->add('save', SubmitType::class, [
      'label' => 'action.save']
    );
  }

  /**
  * {@inheritdoc}
  */
  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults([
      'data_class' => Environment::class,
    ]);
  }

  public function getBlockPrefix()
  {
      return '';
  }
}
