<?php

namespace App\Form\Version;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Version\AppEnvRelated;
use App\Entity\Version\Application;
use App\Entity\Version\Environment;
use App\Repository\Version\ApplicationRepository;
use App\Repository\Version\EnvironmentRepository;

/**
* Defines the form used to create and manipulate a version
*
*/
class VersionType extends AbstractType
{
    private $appliRepo;
    private $envRepo;

    public function __construct(ApplicationRepository $pAppliRepo, EnvironmentRepository $pEnvRepo)
    {
        $this->appliRepo = $pAppliRepo;
        $this->envRepo = $pEnvRepo;
    }

    /**
    * {@inheritdoc}
    */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder->add('application', EntityType::class, [
            // looks for choices from this entity
            'class' => Application::class,
            'query_builder' => $this->appliRepo->getDistinctNames(),
            'choice_label' => 'name',
        ]);

        $builder->add('environment', EntityType::class, [
            // looks for choices from this entity
            'class' => Environment::class,
            'query_builder' => $this->envRepo->getDistinctNames(),
            'choice_label' => 'name',
        ]);

        $builder->add('version', TextType::class);
        $builder->add('link', TextType::class);
        $builder->add('comment', TextareaType::class);
        // champ date avec un calendrier
        $builder->add('date', DateType::class, array(
                    'widget' => 'single_text',
        ));


        // Actions
        $builder->add('save', SubmitType::class, [
            'label' => 'action.save',
        ]);
    }

    /**
    * No data class related:
    * Binding an entity with a composite primary key to a query is not supported
    */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
          'data_class' => AppEnvRelated::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
