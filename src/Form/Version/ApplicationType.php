<?php

namespace App\Form\Version;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Version\Application;
use App\Repository\Admin\ServiceRepository;
use App\Form\Admin\ServiceType;
use App\Entity\Admin\Service;

/**
* Defines the form used to create and manipulate admin applications
*
*/
class ApplicationType extends AbstractType
{
    private $svcRepo;

    public function __construct(ServiceRepository $pServiceRepo)
    {
        $this->svcRepo = $pServiceRepo;
    }
  /**
  * {@inheritdoc}
  */
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $builder->add('name', TextType::class);
    $builder->add('link', TextType::class);
    $builder->add('description', TextareaType::class, [
      'attr' => ['rows' => 4]
    ]);

    // Service related
    $builder->add('service', EntityType::class, [
        // looks for choices from this entity
        'class' => Service::class,
        'query_builder' => $this->svcRepo->getDistinctNames(),
        'choice_label' => 'name',
    ]);

    // Actions
    $builder->add('save', SubmitType::class, [
      'label' => 'action.save',
    ]);
  }

  /**
  * {@inheritdoc}
  */
  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults([
      'data_class' => Application::class,
      'allow_extra_fields' => true,
    ]);
  }

  public function getBlockPrefix()
  {
      return '';
  }
}
