<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use function Symfony\Component\String\u;

class AppFixtures extends Fixture
{
  private $passwordHasher;
  private $slugger;

  public function __construct(UserPasswordHasherInterface $passwordHasher, SluggerInterface $slugger)
  {
      $this->passwordHasher = $passwordHasher;
      $this->slugger = $slugger;
  }

  public function load(ObjectManager $manager): void
  {
      $this->loadUsers($manager);
  }

  private function loadUsers(ObjectManager $manager): void
  {
      foreach ($this->getUserData() as [$fullname, $username, $password, $email, $roles, $enabled]) {
          $user = new User();
          $user->setFullName($fullname);
          $user->setUsername($username);
          $user->setPassword($this->passwordHasher->hashPassword($user, $password));
          $user->setEmail($email);
          $user->setRolesList($roles);
          $user->setActif($enabled);

          $manager->persist($user);
          $this->addReference($username, $user);
      }

      $manager->flush();
  }

  private function getUserData(): array
    {
        return [
            // $userData = [$fullname, $username, $password, $email, $roleList, $enabled];
            ['Mahal Pacha', 'alexadmin', 'alexadmin', 'mahal.pacha+admin@email.fr', 'ROLE_ADMIN', '1']
        ];
    }
}
