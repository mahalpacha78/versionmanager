<?php

namespace App\Twig;

use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TemplateWrapper;
use Twig\TwigFunction;
use Twig\TwigFilter;
use App\Utils\Constants;

class TechnicalExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('boolean_ouinonformat', array($this, 'booleanFilter'))
        );
    }

    public function booleanFilter( $booleanValue)
    {
        if ($booleanValue != Constants::BOOLEAN_FALSE_CODE && $booleanValue != Constants::BOOLEAN_TRUE_CODE)
        throw new \Exception("boolean_ouinonformat is unable to handle other than 1 and 0 values");

        if($booleanValue == Constants::BOOLEAN_FALSE_CODE){
            $resultat = Constants::BOOLEAN_FALSE_AFFICHAGE;
        } else {
            // TechConstantes::BOOLEAN_TRUE_CODE
            $resultat = Constants::BOOLEAN_TRUE_AFFICHAGE;
        }
        return $resultat;
    }
}
