<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\Form\Type\ChangePasswordType;
use App\Form\Account\UserType;
use App\Form\Account\RegisterType;
use App\Utils\StringUtils;
use App\Utils\Constants;
use App\Entity\User;

/**
* Controller used to self manage current user.
*
*/
class UserController extends MainController
{
    /**
    * edit detail of
    * @Template("user/edit.html.twig")
    */
    public function edit(Request $request, EntityManagerInterface $entityManager)
    {
        $user = $this->getUser();
        parent::log(Constants::LOG_LEVEL_DEBUG, "UserController->edit", "User edit profile");
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            parent::log(Constants::LOG_LEVEL_DEBUG, "UserController->edit", "Form valid  and submitted");
            $entityManager->flush();
            $this->addFlash('success', 'user.updated_successfully');
        }

        return array (
            'user' => $user,
            'form' => $form->createView()
        );
    }

    /**
    * Register an account
    * @Template("account/register.html.twig")
    */
    public function register(Request $request, UserPasswordHasherInterface $pHasher, EntityManagerInterface $pEntityMgr)
    {
        parent::log(Constants::LOG_LEVEL_DEBUG, "UserController->register", "User new profile");
        $form = $this->createForm(RegisterType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            parent::log(Constants::LOG_LEVEL_DEBUG, "UserController->register", "Form valid  and submitted");
            $user = new User();
            $user->setFullName(StringUtils::buildFullName($form->get('firstname')->getData(), $form->get('lastname')->getData()));
            $user->setUsername(StringUtils::buildLogin($form->get('firstname')->getData(), $form->get('lastname')->getData()));
            $user->setEmail($form->get('email')->getData());
            $user->setRolesList(Constants::DFT_ROLE);
             $user->setPassword($pHasher->hashPassword($user, $form->get('newPassword')->getData()));
             // Profile created is disabled by default and must be enabled by admin manager
            $user->setActif(false);
            
            // Now stores to database the user created.
            $pEntityMgr->persist($user);
            $pEntityMgr->flush();
            $this->addFlash('success', 'user.created_successfully');
            return $this->redirectToRoute('homepage');
        }

        return array (
            'form' => $form->createView()
        );
    }

    /**
    * @Template("user/change_password.html.twig")
    */
    public function changePassword(Request $request, UserPasswordHasherInterface $hasher, EntityManagerInterface $entityManager)
    {
        $user = $this->getUser();

        $form = $this->createForm(ChangePasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($hasher->hashPassword($user, $form->get('newPassword')->getData()));
            $entityManager->flush();

            return $this->redirectToRoute('security_logout');
        }

        return array (
            'form' => $form->createView()
        );
    }
}
