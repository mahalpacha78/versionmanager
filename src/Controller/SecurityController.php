<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use App\Controller\MainController;

/**
 */
class SecurityController extends MainController
{
    use TargetPathTrait;

    /**
     * Login  feature
     * @Template("account/login.html.twig")
     */
    public function login(Request $request, Security $security, AuthenticationUtils $helper)
    {
        // if user is already logged in, don't display the login page again
        if ($this->getUser()) {
            return $this->redirectToRoute('homepage');
        }

        // last authentication error (if any)
        return array (
          'last_username' => $helper->getLastUsername(),
          'error' => $helper->getLastAuthenticationError()
        );
    }

    /**
     * This is the route the user can use to logout.
     */
    public function logout(): void
    {
        throw new \Exception('Should be intercepted before!');
    }
}
