<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Controller\MainController;
use App\Utils\Constants;
use App\Entity\Version\Application;
use App\Entity\Version\Environment;
use App\Entity\Version\AppEnvRelated;
use App\Form\Search\VersionFormType;


/**
* Controller default
*
*/
class DefaultController extends MainController
{
  /**
  * @Template("default/homepage.html.twig")
  */
  public function index(Request $request, EntityManagerInterface $pEntityManager)
  {
    parent::log(Constants::LOG_LEVEL_DEBUG, "DefaultController->index", "Begin method");
    $version = new AppEnvRelated();
    $form = $this->createForm ( VersionFormType::class );
    // Intercept POST request and form submit
    if ($request->isMethod ( Constants::HTTP_METHODE_POST  )) {
      parent::log(Constants::LOG_LEVEL_INFO, 'DefaultController->index', 'demande POST');
      $form->handleRequest ( $request );
      if ($form->isSubmitted () && $form->isValid ()) {
        $entities = array();
        $searchService = (empty($form->get('service')->getData())) ? Constants::CD_EMPTY : $form->get('service')->getData();
        $searchApplication = (empty($form->get('application')->getData())) ? Constants::CD_EMPTY : $form->get('application')->getData();
        $searchEnvironment = (empty($form->get('environment')->getData())) ? Constants::CD_EMPTY : $form->get('environment')->getData();
        $searchVersion = (empty($form->get('version')->getData())) ? Constants::CD_EMPTY : $form->get('version')->getData();
        parent::log(Constants::LOG_LEVEL_DEBUG, 'DefaultController->index', 'Submit form - Service is : ' . $searchService);
        parent::log(Constants::LOG_LEVEL_DEBUG, 'DefaultController->index', 'Submit form - application is : ' . $searchApplication);
        parent::log(Constants::LOG_LEVEL_DEBUG, 'DefaultController->index', 'Submit form - environment is : ' . $searchEnvironment);
        parent::log(Constants::LOG_LEVEL_DEBUG, 'DefaultController->index', 'Submit form - version is : ' . $searchVersion);
        /** Retrieve versions */
        $entities = $pEntityManager->getRepository ( Constants::REPO_VERSION_APP )->searchVersions (
          $searchApplication , $searchService , $searchEnvironment, $searchVersion );

        }
      }

      if( empty($entities) ){
        $entities = array();
      }

      return array (
        'form' => $form->createView (),
        'entities' => $entities
      );
    }

    /**
    * @Template("default/dashboard.html.twig")
    */
    public function dashboard(EntityManagerInterface $entityManager)
    {
      parent::log(Constants::LOG_LEVEL_DEBUG, "DefaultController->dashboard", "Begin method");

      parent::checkAuthorization(Constants::DFT_ROLE);

      $appsNumber = $entityManager->getRepository(Constants::REPO_VERSION_APP)->countAll();
      $envNumber = $entityManager->getRepository(Constants::REPO_VERSION_ENV)->countAll();
      $verNumber = $entityManager->getRepository(Constants::REPO_VERSION_VERSIONLINK)->countAll();

      return array(
        'apps' => $appsNumber,
        'envs' => $envNumber,
        'ver' => $verNumber
      );
    }

    /**
    * @Template("default/page/policy.html.twig")
    */
    public function policy()
    {
        return array();
    }

    /**
    * @Template("default/page/terms.html.twig")
    */
    public function terms()
    {
        return array();
    }
    /**
    * @Template("default/page/cookies.html.twig")
    */
    public function cookies()
    {
        return array();
    }

  }
