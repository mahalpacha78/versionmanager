<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Psr\Log\LoggerInterface;
use App\Utils\Constants;

/**
* Controller used as wrapper controller
*
*/
class MainController extends AbstractController
{

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
    *
    * @param string $pLevel: DEBUG/INFO/ERROR
    * @param string $pTitle: Title for the log
    * @param string $pMessage: the basic message
    */
    protected function log(string $pLevel, string $pContext, string $pMessage)
    {
        $message = Constants::LOG__BUNDLE_PREFIXE . $pContext . ' - ' . $pMessage;
        switch ($pLevel) {
            case Constants::LOG_LEVEL_INFO:
            return $this->logger->info($message);
            break;
            case Constants::LOG_LEVEL_DEBUG:
            return $this->logger->debug($message);
            break;
            case Constants::LOG_LEVEL_ERROR:
            return $this->logger->error($message);
            break;
        }
    }

    protected function checkAuthorization (string $pRole) {
        self::log(Constants::LOG_LEVEL_DEBUG, 'MainController->checkAuthorization', 'Checking authorization for role: ' . $pRole);
        if ( !$this->isGranted($pRole)) {
            self::log(Constants::LOG_LEVEL_DEBUG, 'MainController->checkAuthorization', 'Authorization failed for role: ' . $pRole);
            header('Location: ' . $this->generateUrl('feature_not_allowed'));
            exit;
        }
    }

    /**
    *
    * @param Request $request
    * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
    * @Template("default/page/forbidden.html.twig")
    */
    public function refusedFeatureAction(Request $request) {
        self::log(Constants::LOG_LEVEL_DEBUG, 'MainController->refusedFeatureAction', 'Entrée de méthode');
        return array ();
    }

}
