<?php
namespace App\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\MainController;
use App\Repository\Admin\ServiceRepository;
use App\Entity\Admin\Service;
use App\Form\Admin\ServiceType;
use App\Utils\Constants;

/**
* Controller used to manage services of a society
*
*/
class ServicesController extends MainController
{
    /**
    * Lists all services of society declared.
    * @Template("admin/service/index.html.twig")
    */
    public function index(ServiceRepository $pSvc)
    {
        $services = $pSvc->filtrerFiches();
        return array(
            'entities' => $services
        );
    }

    /**
    * Create a new services of society
    * @Template("admin/service/new.html.twig")
    */
    public function new(Request $request, EntityManagerInterface $pEntityMgr)
    {
        parent::log(Constants::LOG_LEVEL_DEBUG, "ServicesController->new", "Begin method");
        $oneSvc = new Service();
        $form = $this->createForm(ServiceType::class, $oneSvc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            parent::log(Constants::LOG_LEVEL_DEBUG, "ServicesController->new", "Form valid  and submitted");
            $pEntityMgr->persist($oneSvc);
            $pEntityMgr->flush();
            $this->addFlash('success', 'service.created_successfully');
            return $this->redirectToRoute('admin_services');
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
    * Show detail of services of society
    * @Template("admin/service/show.html.twig")
    */
    public function show( Service $pSvc )
    {
        return array(
            'service' => $pSvc
        );
    }

    /**
    * edit detail of services of society
    * @Template("admin/service/edit.html.twig")
    */
    public function edit( Request $request,  Service $pSvc , EntityManagerInterface $pEntityMgr)
    {
        parent::log(Constants::LOG_LEVEL_DEBUG, "ServicesController->edit", "Begin method");
        $form = $this->createForm(ServiceType::class, $pSvc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            parent::log(Constants::LOG_LEVEL_DEBUG, "ServicesController->edit", "Form valid  and submitted");
            if ( !empty($form->get('date')->getData()) ) {
                $pSvc->setDate($form->get('date')->getData());
            }
            $pEntityMgr->getRepository(Constants::REPO_VERSION_SVC)->updateFiche(
                $pSvc->getId(), $pSvc->getName(), $pSvc->getDescription(),
                $pSvc->getSociety(), $pSvc->getManager(), $pSvc->getDate());
            $pEntityMgr->flush();
            $this->addFlash('success', 'service.updated_successfully');

            return $this->redirectToRoute('admin_services');
        }

        return array(
            'form' => $form->createView(),
            'service' => $pSvc
        );
    }


    /**
    * Delete a service of a society
    */
    public function delete( Request $request, Service $pSvc , EntityManagerInterface $pEntityMgr)
    {
        parent::log(Constants::LOG_LEVEL_DEBUG, "ServicesController->delete", "Begin method");

        $pEntityMgr->remove($pSvc);
        $pEntityMgr->flush();
        $this->addFlash('success', 'service.deleted_successfully');

        return $this->redirectToRoute('admin_services');

    }
}
