<?php
namespace App\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Controller\MainController;
use App\Utils\Constants;
use App\Entity\Version\Application;
use App\Entity\Version\Environnement;

/**
* Controller used to manage admin features
*
*/
class AdminController extends MainController
{

    /**
    * @Template("admin/index.html.twig")
    */
    public function dashboard(EntityManagerInterface $entityManager)
    {
        $userNumber = $entityManager->getRepository(Constants::REPO_VERSION_USERS)->countUsers(Constants::ROLE_USER);
        $admUsersNumber = $entityManager->getRepository(Constants::REPO_VERSION_USERS)->countUsers(Constants::ROLE_ADMIN);
        $servicesNumber = $entityManager->getRepository(Constants::REPO_VERSION_SVC)->countAll();

        return array(
            'users' => $userNumber,
            'admins' => $admUsersNumber,
            'svc'  => $servicesNumber
        );
    }

    /**
    * @Template("admin/infos.html.twig")
    */
    public function infos()
    {
        return array();
    }

}
