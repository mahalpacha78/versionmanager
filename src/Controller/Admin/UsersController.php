<?php
namespace App\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\UserRepository;
use App\Entity\User;
use App\Form\Admin\UserType;
use App\Utils\Constants;

/**
 * Controller used to manage users and rights
 *
 */
class UsersController extends AdminController
{

  /**
  * Lists all environments declared.
  * @Template("admin/users/index.html.twig")
  */
  public function index(UserRepository $envs)
  {
    $users = $envs->filtrerFiches();
    return array(
      'entities' => $users
    );
  }

  /**
  * Show detail of
  * @Template("admin/users/show.html.twig")
  */
  public function show( User $pUser )
  {
    return array(
      'user' => $pUser
    );
  }

  /**
  * edit detail of
  * @Template("admin/users/edit.html.twig")
  */
  public function edit( Request $request,  User $pUser , EntityManagerInterface $pEntityMgr)
  {
    parent::log(Constants::LOG_LEVEL_DEBUG, "UsersController->edit", "Begin method");
    $form = $this->createForm(UserType::class, $pUser);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      parent::log(Constants::LOG_LEVEL_DEBUG, "UsersController->edit", "Form valid  and submitted");
      // Now update the user file
      $pEntityMgr->getRepository(Constants::REPO_VERSION_USERS)->updateFiche(
        $pUser->getId(),
        $pUser->getUsername(),
        $pUser->getFullName(),
        $pUser->getEmail(),
        $pUser->getRolesList()
      );

      $pEntityMgr->flush();
      $this->addFlash('success', 'user.updated_successfully');

      return $this->redirectToRoute('admin_users_index');
    }

    return array(
      'form' => $form->createView(),
      'user' => $pUser
    );
  }

  /**
  * Delete environment
  */
  public function delete( Request $request,  User $pUser , EntityManagerInterface $pEntityMgr)
  {
    parent::log(Constants::LOG_LEVEL_DEBUG, "UsersController->delete", "Begin method");

    $pEntityMgr->remove($pUser);
    $pEntityMgr->flush();
    $this->addFlash('success', 'user.deleted_successfully');

    return $this->redirectToRoute('admin_users_index');
  }
}
