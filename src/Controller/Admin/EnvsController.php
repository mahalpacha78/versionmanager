<?php
namespace App\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Controller\MainController;
use App\Repository\Version\EnvironmentRepository;
use App\Entity\Version\Environment;
use App\Form\Version\EnvironmentType;
use App\Utils\Constants;

/**
* Controller used to manage admin env
*
*/
class EnvsController extends MainController
{

    /**
    * Lists all environments declared.
    * @Template("admin/environment/index.html.twig")
    */
    public function index(EnvironmentRepository $envs)
    {
        $environments = $envs->filtrerFiches();
        return array(
            'entities' => $environments
        );
    }

    /**
    * Create a new environment
    * @Template("admin/environment/new.html.twig")
    */
    public function new(Request $request, EntityManagerInterface $pEntityMgr)
    {
        parent::log(Constants::LOG_LEVEL_DEBUG, "EnvsController->new", "Begin method");
        $oneEnvironnement = new Environment();
        $form = $this->createForm(EnvironmentType::class, $oneEnvironnement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            parent::log(Constants::LOG_LEVEL_DEBUG, "EnvsController->new", "Form valid  and submitted");
            $pEntityMgr->persist($oneEnvironnement);
            $pEntityMgr->flush();
            $this->addFlash('success', 'environnment.created_successfully');
            return $this->redirectToRoute('admin_environnements');
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
    * Show detail of
    * @Template("admin/environment/show.html.twig")
    */
    public function show( Environment $pEnvironnement )
    {
        return array(
            'env' => $pEnvironnement
        );
    }

    /**
    * edit detail of
    * @Template("admin/environment/edit.html.twig")
    */
    public function edit( Request $request,  Environment $pEnvironnement , EntityManagerInterface $pEntityMgr)
    {
        parent::log(Constants::LOG_LEVEL_DEBUG, "EnvsController->edit", "Begin method");
        $form = $this->createForm(EnvironmentType::class, $pEnvironnement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            parent::log(Constants::LOG_LEVEL_DEBUG, "EnvsController->edit", "Form valid  and submitted");
            $pEntityMgr->getRepository(Constants::REPO_VERSION_ENV)->updateFiche(
                $pEnvironnement->getId(), $pEnvironnement->getName(), $pEnvironnement->getDescription(),
                $pEnvironnement->getMachine());
                $pEntityMgr->flush();
                $this->addFlash('success', 'environnment.updated_successfully');

                return $this->redirectToRoute('admin_environnements');
            }

            return array(
                'form' => $form->createView(),
                'env' => $pEnvironnement
            );
        }

        /**
        * Delete environment
        */
        public function delete( Request $request,  Environment $pEnvironnement , EntityManagerInterface $pEntityMgr)
        {
            parent::log(Constants::LOG_LEVEL_DEBUG, "EnvsController->delete", "Begin method");

            $pEnvironnement->getEnvapplication()->clear();

            $pEntityMgr->remove($pEnvironnement);
            $pEntityMgr->flush();
            $this->addFlash('success', 'environnment.deleted_successfully');

            return $this->redirectToRoute('admin_environnements');

        }
    }
