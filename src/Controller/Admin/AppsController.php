<?php
namespace App\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\MainController;
use App\Repository\Version\ApplicationRepository;
use App\Entity\Version\Application;
use App\Form\Version\ApplicationType;
use App\Utils\Constants;

/**
* Controller used to manage applications lists features
*
*/
class AppsController extends MainController
{
  /**
  * Lists all environments declared.
  * @Template("admin/application/index.html.twig")
  */
  public function index(ApplicationRepository $apps)
  {
    $applications = $apps->filtrerFiches();
    return array(
      'entities' => $applications
    );
  }

  /**
  * Create a new application
  * @Template("admin/application/new.html.twig")
  */
  public function new(Request $request, EntityManagerInterface $pEntityMgr)
  {
    parent::log(Constants::LOG_LEVEL_DEBUG, "AppsController->new", "Begin method");
    $oneApplication = new Application();
    $form = $this->createForm(ApplicationType::class, $oneApplication);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      parent::log(Constants::LOG_LEVEL_DEBUG, "AppsController->new", "Form valid  and submitted");
      $pEntityMgr->persist($oneApplication);
      $pEntityMgr->flush();
      $this->addFlash('success', 'application.created_successfully');
      return $this->redirectToRoute('admin_applications');
    }

    return array(
      'form' => $form->createView()
    );
  }

  /**
  * Show detail of
  * @Template("admin/application/show.html.twig")
  */
  public function show( Application $pAppli )
  {
    return array(
      'appli' => $pAppli
    );
  }

  /**
  * edit detail of
  * @Template("admin/application/edit.html.twig")
  */
  public function edit( Request $request,  Application $pAppli , EntityManagerInterface $pEntityMgr)
  {
    parent::log(Constants::LOG_LEVEL_DEBUG, "AppsController->edit", "Begin method");
    $form = $this->createForm(ApplicationType::class, $pAppli);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      parent::log(Constants::LOG_LEVEL_DEBUG, "AppsController->edit", "Form valid  and submitted");
      $pEntityMgr->getRepository(Constants::REPO_VERSION_APP)->updateFiche( $pAppli->getId(), $pAppli->getName(), $pAppli->getDescription(), $pAppli->getLink() );
      $pEntityMgr->flush();
      $this->addFlash('success', 'application.updated_successfully');

      return $this->redirectToRoute('admin_applications');
    }

    return array(
      'form' => $form->createView(),
      'appli' => $pAppli
    );
  }

  /**
  * Delete application
  */
  public function delete( Request $request,  Application $pAppli , EntityManagerInterface $pEntityMgr)
  {
    parent::log(Constants::LOG_LEVEL_DEBUG, "AppsController->delete", "Begin method");

    $pAppli->getApplicationenvs()->clear();

    $pEntityMgr->remove($pAppli);
    $pEntityMgr->flush();
    $this->addFlash('success', 'application.deleted_successfully');

    return $this->redirectToRoute('admin_applications');

  }
}
