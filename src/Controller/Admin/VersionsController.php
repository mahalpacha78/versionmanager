<?php
namespace App\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\MainController;
use App\Repository\Version\ApplicationRepository;
use App\Repository\Version\EnvironmentRepository;
use App\Entity\Version\Application;
use App\Entity\Version\AppEnvRelated;
use App\Entity\Version\Environment;
use App\Form\Version\VersionType;
use App\Utils\Constants;

/**
* Controller used to manage versions of applications in an environnement
*
*/
class VersionsController extends MainController
{
  /**
  * Lists all versions declared.
  * @Template("admin/version/index.html.twig")
  */
  public function index(ApplicationRepository $apps)
  {
    $applications = $apps->filtrerVersions();
    return array(
      'entities' => $applications
    );
  }

  /**
  * Create a new application
  * @Template("admin/version/new.html.twig")
  */
  public function new(Request $request, EntityManagerInterface $pEntityMgr, ApplicationRepository $pAppliRepo, EnvironmentRepository $pEnvRepo)
  {
    parent::log(Constants::LOG_LEVEL_DEBUG, "VersionsController->new", "Begin method");
    //$oneApplication = new Application();
    $form = $this->createForm(VersionType::class);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      parent::log(Constants::LOG_LEVEL_DEBUG, "VersionsController->new", "Form valid  and submitted");
      //$pEntityMgr->persist($oneApplication);
      parent::log(Constants::LOG_LEVEL_DEBUG, "VersionsController->new", "Form application id: " . $form->get('application')->getData());
      parent::log(Constants::LOG_LEVEL_DEBUG, "VersionsController->new", "Form environment id: " . $form->get('environment')->getData());
      parent::log(Constants::LOG_LEVEL_DEBUG, "VersionsController->new", "Form version: " . $form->get('version')->getData());
      $application  = $pAppliRepo->getApplicationByName($form->get('application')->getData());
      $environment  = $pEnvRepo->getApplicationByName($form->get('environment')->getData());

      // Relation alimentation
      $linkedVersion = new AppEnvRelated();
      $linkedVersion->setVersion($form->get('version')->getData());
      $linkedVersion->setComment($form->get('comment')->getData());
      if ( !empty($form->get('date')->getData()) ) {
          $linkedVersion->setDate($form->get('date')->getData());
      }
      $linkedVersion->setApplication($application);
      $linkedVersion->setEnvironment($environment);
      $pEntityMgr->persist($linkedVersion);

      $pEntityMgr->flush();
      $this->addFlash('success', 'version.created_successfully');
      return $this->redirectToRoute('admin_versions');
    }

    return array(
      'form' => $form->createView()
    );
  }

  /**
  * Show detail of
  * @Template("admin/version/show.html.twig")
  */
  public function show( $id , $envid, ApplicationRepository $pAppliRepo)
  {
      $environment = new Environment();
      $application = new Application();
      if (! empty ($id) && ! empty ($envid)) {
          $application  = $pAppliRepo->getVersionByIds($id, $envid);
          $link = $application->getApplicationenvs()->first();
          $environment = $link->getEnvironment();
      }
    return array(
      'appli'   => $application,
      'link'    => $link,
      'env'     => $environment
    );
  }

  /**
  * edit detail of
  * @Template("admin/version/edit.html.twig")
  */
  public function edit( Request $request, $id , $envid, ApplicationRepository $pAppliRepo, EntityManagerInterface $pEntityMgr )
  {
    parent::log(Constants::LOG_LEVEL_DEBUG, "VersionsController->edit", "Begin method");
    $environment = new Environment();
    $application = new Application();
    $link = new AppEnvRelated();
    if (! empty ($id) && ! empty ($envid)) {
        $application  = $pAppliRepo->getVersionByIds($id, $envid);
        $link = $application->getApplicationenvs()->first();
        $environment = $link->getEnvironment();
    }
    $form = $this->createForm(VersionType::class, $link);

    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      parent::log(Constants::LOG_LEVEL_DEBUG, "VersionsController->edit", "Form valid  and submitted");
      $link->setVersion($form->get('version')->getData());
      $link->setComment($form->get('comment')->getData());
      if ( !empty($form->get('date')->getData()) ) {
          $link->setDate($form->get('date')->getData());
      }
      $pEntityMgr->getRepository(Constants::REPO_VERSION_VERSIONLINK)->updateFiche( 
          $id,
          $envid,
          $link->getVersion(),
          $link->getComment(),
          $link->getDate(), 
          $link->getLink()
      );
      $pEntityMgr->flush();
      
      $this->addFlash('success', 'version.updated_successfully');

      return $this->redirectToRoute('admin_versions');
    }

    return array(
      'form' => $form->createView()
    );
  }

  /**
  * Delete version
  */
  public function delete( Request $request,  $id , $envid, ApplicationRepository $pAppliRepo)
  {
    parent::log(Constants::LOG_LEVEL_DEBUG, "VersionsController->delete", "Begin method");

    $pAppliRepo->deleteVersion($id, $envid);
    $this->addFlash('success', 'version.deleted_successfully');

    return $this->redirectToRoute('admin_versions');

  }
}
