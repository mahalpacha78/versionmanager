<?php
namespace App\Utils;

/**
* This class is used to provide static constants used in the project
*
*/
class Constants
{
    const CD_LISTE_VALUE_ALL = '*';
    const CD_EMPTY = '';
    /** Defaults roles granted or not */
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_USER = 'ROLE_USER';
    const DFT_ROLE = self::ROLE_USER;
    /** Constantes dediées aux log, implique le niveau de trace */
    const LOG_LEVEL_DEBUG = 'DEBUG';
    const LOG_LEVEL_INFO = 'INFO';
    const LOG_LEVEL_ERROR = 'ERROR';
    const LOG__BUNDLE_PREFIXE = 'Application Version Mgr -->';
    /** Methodes type http */
    const HTTP_METHODE_POST = 'POST';
    const HTTP_METHODE_GET = 'GET';
    const HTTP_METHODE_PUT = 'PUT';
    const HTTP_METHODE_DELETE = 'DELETE';
    const HTTP_METHODE_PATCH = 'PATCH';

    /** Codes retour http */
    const HTTP_STATUS_200 = 200;

    // To be defined ....
    const REPO_VERSION_APP = "App\Entity\Version\Application";
    const REPO_VERSION_ENV = "App\Entity\Version\Environment";
    const REPO_VERSION_SVC = "App\Entity\Admin\Service";
    const REPO_VERSION_VERSIONLINK = "App\Entity\Version\AppEnvRelated";
    const REPO_VERSION_USERS = "App\Entity\User";

    // Separators
    const TWIG_SEPARATOR = ':';
    const LIST_SEPARATOR = ',';

    // Technical changes , visual view
    const BOOLEAN_TRUE_INFO = true;
    const BOOLEAN_TRUE_INFO_STRING = "true";
    const BOOLEAN_TRUE_CODE = '1';
    const BOOLEAN_TRUE_VALUE = 'VRAI';
    const BOOLEAN_TRUE_AFFICHAGE = 'Oui';
    const BOOLEAN_FALSE_INFO = false;
    const BOOLEAN_FALSE_INFO_STRING = "false";
    const BOOLEAN_FALSE_CODE = '0';
    const BOOLEAN_FALSE_VALUE = 'FAUX';
    const BOOLEAN_FALSE_AFFICHAGE = 'Non';
    const BOOLEAN_TRUE_STRING = '1';
    const BOOLEAN_FALSE_STRING = '0';
}
