<?php
namespace App\Utils;

/**
 * This class is used to provide functions string
 *
 */
class StringUtils
{

    /**
     * upper case function
     */
    public function upper(string $pValue)
    {
        return strtoupper($pValue);
    }

    /**
     * lower case function
     */
    public function lower(string $pValue)
    {
        return strtolower($pValue);
    }

    /**
     * upper case first letter
     */
    private function upperFirstLetter(string $pValue)
    {
        return ucfirst($pValue);
    }

    /**
     * first letter of a word
     */
    private function firstLetter(string $pValue)
    {
        return substr($pValue, 0, 1);
    }

    /**
     * upper case function
     */
    public function buildFullName(string $pFirstName, string $pLastName)
    {
        return self::upperFirstLetter($pFirstName) . " " . self::upper($pLastName);
    }
    /**
     * Login extract from First name and last name
     */
    public function buildLogin(string $pFirstName, string $pLastName)
    {
        return self::lower(self::firstLetter($pFirstName) . $pLastName);
    }

}
