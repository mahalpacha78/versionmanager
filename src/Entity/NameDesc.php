<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
* @MappedSuperclass
*
*/
class NameDesc
{
  public function __toString(): string
  {
    return $this->name;
  }
  
  /**
  * @var string
  *
  * @ORM\Column(type="string")
  * @Assert\NotBlank()
  */
  private $name;
  
  /**
  * @var string
  *
  * @ORM\Column(type="string")
  * @Assert\Length(min=2, max=250)
  */
  private $description;
  
  /**
  * Get the value of Name 
  * 
  * @return string
  */
  public function getName(): ?string
  {
    return $this->name;
  }
  
  /** 
  * Set the value of Name 
  * 
  * @param string $pName
  */
  public function setName(string $pName): void
  {
    $this->name = $pName;
  }
  
  /**
  * Get the value of Description 
  * 
  * @return string
  */
  public function getDescription(): ?string
  {
    return $this->description;
  }
  
  /** 
  * Set the value of Description 
  * 
  * @param string $pDescription
  * 
  * @return self
  */
  public function setDescription($pDescription): void
  {
    $this->description = $pDescription;
  }
  
  
}
