<?php
namespace App\Entity\Admin;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Entity\NameDesc;
use App\Entity\Version\Application;

/**
* @ORM\Entity(repositoryClass="App\Repository\Admin\ServiceRepository")
* @ORM\Table(name="service")
*
*/
class Service extends NameDesc
{

  public function __construct()
  {
    $this->date = new \DateTimeImmutable();
    $this->applications = new ArrayCollection();
  }

  public function __toString(): string
  {
    return parent::__toString();
  }

  /**
  * @var int
  * @ORM\Column(type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  private $id;

  /**
  * @var string
  * @ORM\Column(type="string")
  */
  private $society;

  /**
  * @var string
  * @ORM\Column(type="string")
  */
  private $manager;

  /**
  * @var \DateTime
  *
  * @ORM\Column(type="datetime")
  */
  private $date;

  /**
  * @var Environment[]|Collection
  *
  * @ORM\OneToMany(
  *      targetEntity="App\Entity\Version\Application",
  *      mappedBy="service")
  * )
  */
  private $applications;

  /**
  * Get the value of Id
  *
  * @return int
  */
  public function getId(): ?int
  {
    return $this->id;
  }

  /**
  * Set the value of Id
  *
  * @param int $id
  *
  * @return self
  */
  public function setId($id): void
  {
    $this->id = $id;
  }

  /**
  * Get the value of Society
  *
  * @return string
  */
  public function getSociety(): ?string
  {
    return $this->society;
  }

  /**
  * Set the value of Society
  *
  * @param string $society
  */
  public function setSociety(string $pSociety)
  {
    $this->society = $pSociety;
  }

  /**
  * Get the value of Manager
  *
  * @return string
  */
  public function getManager(): string
  {
    return $this->manager;
  }

  /**
  * Set the value of Manager
  *
  * @param string $manager
  */
  public function setManager(string $pManager)
  {
    $this->manager = $pManager;

    return $this;
  }

  /**
  * Get the value of Date
  *
  * @return \DateTime
  */
  public function getDate()
  {
    return $this->date;
  }

  /**
  * Set the value of Date
  *
  * @param \DateTime $pDate
  */
  public function setDate(\DateTime $pDate)
  {
    $this->date = $pDate;
  }

  /**
  * Get the value of Application
  *
  * @return Application[]|Collection
  */
  public function getApplications() : Collection
  {
    return $this->applications;
  }

  public function addApplication(Application $pApp): void
  {
    if (!$this->applications->contains($pApp)) {
      $this->applications->add($pApp);
    }
  }

  public function removeApplication(Application $pApp): void
  {
    $this->applications->removeElement($pApp);
  }

}
