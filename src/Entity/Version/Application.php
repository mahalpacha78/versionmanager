<?php
namespace App\Entity\Version;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Entity\NameDesc;
use App\Entity\Admin\Service;

/**
* @ORM\Entity(repositoryClass="App\Repository\Version\ApplicationRepository")
* @ORM\Table(name="application")
*
*/
class Application extends  NameDesc
{
  public function __toString(): string
  {
    return parent::__toString();
  }

  public function __construct()
  {
    $this->applicationenvs = new ArrayCollection();
  }

  /**
  * @var int
  * @ORM\Column(type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  private $id;

  /**
  * @var string
  *
  * @ORM\Column(type="string")
  */
  private $link;

  /**
  * @var Environment[]|Collection
  *
  * @ORM\OneToMany(
  *      targetEntity="AppEnvRelated",
  *      mappedBy="application")
  * )
  */
  private $applicationenvs;

  /**
  * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Service", inversedBy="applications")
  * @ORM\JoinColumn(name="service_id", referencedColumnName="id", nullable=false)
  */
  private $service;

  /**
  * Get the value of Id
  *
  * @return int
  */
  public function getId(): ?int
  {
    return $this->id;
  }

  /**
  * Set the value of Id
  *
  * @param int $id
  *
  * @return self
  */
  public function setId($id): void
  {
    $this->id = $id;
  }

  /**
  * Get the value of Link
  *
  * @return string
  */
  public function getLink(): ?string
  {
    return $this->link;
  }

  /**
  * Set the value of Link
  *
  * @param string $link
  */
  public function setLink($pLink): void
  {
    $this->link = $pLink;
  }

  /**
  * Get the value of Applicationenvs
  *
  * @return Environment[]|Collection
  */
  public function getApplicationenvs() : Collection
  {
    return $this->applicationenvs;
  }

  public function addApplicationEnvs(AppEnvRelated $pEnv): void
  {
    if (!$this->applicationenvs->contains($pEnv)) {
      $this->applicationenvs->add($pEnv);
    }
  }

  public function removeEnvapplication(AppEnvRelated $pEnv): void
  {
    $this->applicationenvs->removeElement($pEnv);
  }

  /**
  * Get the value of Service
  *
  * @return mixed
  */
  public function getService()
  {
      return $this->service;
  }

  /**
  * Set the value of Service
  *
  * @param mixed $application
  */
  public function setService($pService) : void
  {
      $this->service = $pService;
  }

}
