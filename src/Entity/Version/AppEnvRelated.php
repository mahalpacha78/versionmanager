<?php
namespace App\Entity\Version;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
* @ORM\Entity(repositoryClass="App\Repository\Version\VersionRepository")
* @ORM\Table(name="application_environnement")
*
*/
class AppEnvRelated
{

    public function __construct()
    {
        $this->date = new \DateTimeImmutable();
    }

    /**
    * @var string
    *
    * @ORM\Column(type="string")
    */
    private $version;

    /**
    * @var string
    *
    * @ORM\Column(type="string")
    */
    private $comment;

    /**
    * @var \DateTime
    *
    * @ORM\Column(type="datetime")
    */
    private $date;
    
    /**
    * @var string
    *
    * @ORM\Column(type="string")
    */
    private $link;

    /**
    * @ORM\Id()
    * @ORM\ManyToOne(targetEntity="Application", inversedBy="applicationenvs")
    * @ORM\JoinColumn(name="appli_id", referencedColumnName="id", nullable=false)
    */
    private $application;

    /**
    * @ORM\Id()
    * @ORM\ManyToOne(targetEntity="Environment", inversedBy="envapplication")
    * @ORM\JoinColumn(name="env_id", referencedColumnName="id", nullable=false)
    */
    private $environment;




    /**
    * Get the value of Version
    *
    * @return string
    */
    public function getVersion() : string
    {
        return $this->version;
    }

    /**
    * Set the value of Version
    *
    * @param string $version
    *
    * @return self
    */
    public function setVersion(string $version) : void
    {
        $this->version = $version;
    }

    /**
    * Get the value of Comment
    *
    * @return string
    */
    public function getComment() : string
    {
        return $this->comment;
    }

    /**
    * Set the value of Comment
    *
    * @param string $comment
    *
    * @return self
    */
    public function setComment(string $comment) : void
    {
        $this->comment = $comment;
    }

    /**
    * Get the value of Date
    *
    * @return \DateTime
    */
    public function getDate()
    {
        return $this->date;
    }

    /**
    * Set the value of Date
    *
    * @param \DateTime $date
    *
    * @return self
    */
    public function setDate(\DateTime $date) : void
    {
        $this->date = $date;
    }

    /**
    * Get the value of Application
    *
    * @return mixed
    */
    public function getApplication()
    {
        return $this->application;
    }

    /**
    * Set the value of Application
    *
    * @param mixed $application
    *
    * @return self
    */
    public function setApplication($application) : void
    {
        $this->application = $application;
    }

    /**
    * Get the value of Environment
    *
    * @return mixed
    */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
    * Set the value of Environment
    *
    * @param mixed $environment
    */
    public function setEnvironment($environment) : void
    {
        $this->environment = $environment;
    }
    
    /**
    * Get the value of Link
    *
    * @return string
    */
    public function getLink(): ?string
    {
      return $this->link;
    }
  
    /**
    * Set the value of Link
    *
    * @param string $link
    */
    public function setLink($pLink): void
    {
      $this->link = $pLink;
    }


}
