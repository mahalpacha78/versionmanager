<?php
namespace App\Entity\Version;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use  App\Entity\NameDesc;

/**
* @ORM\Entity(repositoryClass="App\Repository\Version\EnvironmentRepository")
* @ORM\Table(name="environment")
*
*/
class Environment extends NameDesc
{

  public function __toString(): string
  {
    return parent::__toString();
  }

  public function __construct()
  {
    $this->envapplication = new ArrayCollection();
  }

  /**
  * @var int
  *
  * @ORM\Id
  * @ORM\Column(name="id", type="integer")
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  private $id;

  /**
  * @var string
  *
  * @ORM\Column(type="string")
  */
  private $machine;

  /**
  * @var Application[]|Collection
  *
  * @ORM\OneToMany(targetEntity="AppEnvRelated", mappedBy="environment")
  */
  private $envapplication;

  /**
  * Get the value of Id
  *
  * @return int
  */
  public function getId(): ?int
  {
    return $this->id;
  }

  /**
  * Set the value of Id
  *
  * @param int $id
  *
  * @return self
  */
  public function setId($id): void
  {
    $this->id = $id;
  }

  /**
  * Get the value of Machine
  *
  * @return string
  */
  public function getMachine(): ?string
  {
    return $this->machine;
  }

  /**
  * Set the value of Machine
  *
  * @param string $machine
  *
  * @return self
  */
  public function setMachine($machine): void
  {
    $this->machine = $machine;
  }

  /**
  * Get the value of Envapplication
  *
  * @return Application
  */
  public function getEnvapplication() : Collection
  {
    return $this->envapplication;
  }

  public function addEnvapplication(Application $pAppli): void
  {
    if (!$this->envapplication->contains($pAppli)) {
      $this->envapplication->add($pAppli);
    }
  }

  public function removeEnvapplication(Application $pAppli): void
  {
    $this->envapplication->removeElement($pAppli);
  }


}
