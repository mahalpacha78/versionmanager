<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Entity(repositoryClass="App\Repository\UserRepository")
* @ORM\Table(name="user")
*
*/
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
  /**
  * @var int
  *
  * @ORM\Id
  * @ORM\GeneratedValue
  * @ORM\Column(type="integer")
  */
  private $id;

  /**
  * @var string
  *
  * @ORM\Column(type="string")
  * @Assert\NotBlank()
  */
  private $fullName;

  /**
  * @var string
  *
  * @ORM\Column(type="string", unique=true)
  * @Assert\NotBlank()
  * @Assert\Length(min=2, max=50)
  */
  private $username;

  /**
  * @var string
  *
  * @ORM\Column(type="string", unique=true)
  * @Assert\Email()
  */
  private $email;

  /**
  * @var string
  *
  * @ORM\Column(type="string")
  */
  private $password;

  /**
  * @var string
  *
  * @ORM\Column(type="string", name="roles_list")
  */
  private $rolesList;

  /**
   * @ORM\Column(type="boolean")
   */
  protected $actif;

  /**
  * @var array
  *
  */
  private $roles = [];

  public function getId(): ?int
  {
    return $this->id;
  }

  public function setFullName(string $fullName): void
  {
    $this->fullName = $fullName;
  }

  public function getFullName(): ?string
  {
    return $this->fullName;
  }

  public function getUserIdentifier(): string
  {
    return $this->username;
  }

  public function getUsername(): string
  {
    return $this->getUserIdentifier();
  }

  public function setUsername(string $username): void
  {
    $this->username = $username;
  }

  public function getEmail(): ?string
  {
    return $this->email;
  }

  public function setEmail(string $email): void
  {
    $this->email = $email;
  }

  public function getPassword(): ?string
  {
    return $this->password;
  }

  public function setPassword(string $password): void
  {
    $this->password = $password;
  }

  /**
  * Returns the roles or permissions granted to the user for security.
  */
  public function getRoles(): array
  {
    $roles = explode (",", $this->rolesList);

    // guarantees that a user always has at least one role for security
    if (empty($roles)) {
      $roles[] = 'ROLE_USER';
    }

    return array_unique($roles);
  }

  public function setRoles(array $roles): void
  {
    $this->roles = $roles;
  }

  /**
  * Returns the salt that was originally used to encode the password.
  *
  * {@inheritdoc}
  */
  public function getSalt(): ?string
  {
    // We're using bcrypt in security.yaml to encode the password, so
    // the salt value is built-in and you don't have to generate one
    // See https://en.wikipedia.org/wiki/Bcrypt

    return null;
  }

  /**
  * Removes sensitive data from the user.
  *
  * {@inheritdoc}
  */
  public function eraseCredentials(): void
  {
    // if you had a plainPassword property, you'd nullify it here
    // $this->plainPassword = null;
  }

  public function __serialize(): array
  {
    // add $this->salt too if you don't use Bcrypt or Argon2i
    return [$this->id, $this->username, $this->password];
  }

  public function __unserialize(array $data): void
  {
    // add $this->salt too if you don't use Bcrypt or Argon2i
    [$this->id, $this->username, $this->password] = $data;
  }

  /**
  * Get the value of Roles List
  *
  * @return string
  */
  public function getRolesList() : string
  {
    return $this->rolesList;
  }

  /**
  * Get the value of Roles List
  *
  */
  public function setRolesList(string $pRolesList) : void
  {
    $this->rolesList = $pRolesList;
  }

  /**
  * Set the value of Roles List
  *
  * @param string $rolesList
  *
  * @return self
  */
  public function setRolesFromRolesList(array $pRolesInList) : void
  {
    $roles = implode (",", $pRolesInList);
    $this->rolesList = $roles;
  }

    /**
     * Get the value of Actif
     *
     * @return mixed
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set the value of Actif
     *
     * @param mixed $actif
     *
     * @return self
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

}
